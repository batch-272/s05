1. 
SELECT customerName FROM customers WHERE country = "Philippines";

2.
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

3. 
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

4.
SELECT firstName, lastName FROM employees WHERE email="jfirrelli@classicmodelcars.com";

5.
SELECT customerName FROM customers WHERE state IS NULL;

6.
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" and firstName = "Steve";

7.
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

8.
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

9.
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

10.
SELECT DISTINCT country FROM customers;

11.
SELECT DISTINCT status FROM orders;

12.
SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

13.
SELECT e.firstName, e.lastName, o.city 
FROM employees AS e JOIN offices AS o 
ON e.officeCode = o.officeCode AND o.city = "Tokyo";

14. 
SELECT c.customerName
FROM customers AS c JOIN employees AS e
ON c.salesRepEmployeeNumber = e.employeeNumber AND e.lastName = "Thompson" AND e.firstName = "Leslie";

15.
SELECT c.customerName, p.productName
FROM customers AS c 
JOIN orders AS o ON c.customerNumber = o.customerNumber
JOIN orderdetails as od ON o.orderNumber = od.orderNumber
JOIN products as p ON od.productCode = p.productCode
WHERE c.customerName = "Baane Mini Imports";

16.
SELECT e.firstName, e.lastName, o.country, c.customerName
FROM employees as e JOIN offices as o ON e.officeCode = o.officeCode
JOIN customers as c ON c.salesRepEmployeeNumber = e.employeeNumber
WHERE o.country = c.country;

17.
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;

18.
SELECT customerName FROM customers WHERE phone LIKE "%+81%";